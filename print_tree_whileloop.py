#!/usr/bin/python2.7
import sys
def standard_output(input_para):
	sys.stdout.write(input_para)

def print_tree(_level_num):
	### Crown ###
	each_level_C = 0
	while each_level_C < _level_num:
		space_before_C, count_star_C = 0, 0
		while space_before_C < (_level_num - each_level_C - 1):
			standard_output(" ")
			space_before_C += 1
		while count_star_C < (2*each_level_C + 1):
			standard_output("*")
			count_star_C += 1
		standard_output("\n")
		each_level_C += 1
	### Trunk ###
	each_level_T = 0
	space_before_T = ((2*_level_num - 1) - (_level_num*3/4))/2
	while each_level_T < (_level_num/3):
		standard_output(" "*space_before_T + "*"*(_level_num*3/4) + "\n")
		each_level_T += 1
def main():
	level_num = input("How many tree_level ?\n")
	print_tree(level_num)
	return 0


if __name__ == '__main__':
	check = main()
	sys.exit(check)








