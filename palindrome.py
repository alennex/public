import os
import sys
import re

def turn_input_string(input_string):
    turn_string = input_string[::-1]
    return turn_string


def if_two_string_match(input_string, turn_string):
    if re.match(input_string, turn_string):
        return True
    else:
        return False


def main():
    input_string = raw_input('Input a list ??\n>>')
    turn_string = turn_input_string(input_string)
    match_bool = if_two_string_match(input_string, turn_string)
    print input_string, "is palindrome ??? -->", match_bool
    return 0


if __name__ == '__main__':
    rc = main()
    sys.exit(rc)


