git init
git config --global user.name 'alennex'
git config --global user.email 'alennex@gmail.com'

git remote add [remote_repository] [remote_URL] -> add an repository
git fetch [remote_name] -> synch remote repository

git push [remote_branch] [local_branch] -> delete remote_branch
git pull [remote_branch] [locka_branch] -> combol remote branch to local_branch
git push    -> mean push local_branch to remote

git config --list
    user.email=alennex@gmail.com
    user.name=alennex
    color.ui=true
    core.repositoryformatversion=0
    core.filemode=true
    core.bare=false
    core.logallrefupdates=true

git clone [URL] [Document_Name]

git add [Program_Name]  -> push [Program_Name] on stage, on tracking...
git add .   -> push [Program_just_rewrite] on stage, on Tracking...
git revert  -> take out [Program_just_add]

git commit -m 'write_something_comment...'  -> one commit seen one saved
    -m mean commit hurry_commit,
    -a mean add all file and do commit,
    -v mean show all change_record

gitk --all  -> open git GUI mode

git branch  -> tell what branch you in
git checkout [branch_name]  -> change to [branch_name]



