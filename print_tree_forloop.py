#!/usr/bin/python2.7
import sys
def standard_output(input_para):
	sys.stdout.write(input_para)

def print_tree(_level_num):
	### Crown ###
	for each_level_C in range(_level_num):
		for space_before_C in range(_level_num - each_level_C - 1):
			standard_output(" ")
		for count_star_C in range(2*each_level_C + 1):
			standard_output("*")
		standard_output("\n")
	### Trunk ###
	space_before_T = ((2*_level_num - 1) - (_level_num*3/4))/2
	for each_level_T in range(_level_num/3):
		standard_output(" "*space_before_T + "*"*(_level_num*3/4) + "\n")
def main():
	level_num = input("How many tree_level ?\n")
	print_tree(level_num)
	return 0


if __name__ == '__main__':
	check = main()
	sys.exit(check)








